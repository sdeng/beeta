(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['signup-form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  


  return "{% if account %}\n    <p>Beta sign-up for {{ account.email }} complete! We'll email you when {{ account.client.name }} is ready.</p>\n{% else %}\n    <form id=\"beeta-email-signup\" class=\"beeta-form\" method=\"post\">\n        {% if form.non_field_errors %}\n            {% for error in form.non_field_errors %}\n            <p>{{ error|escape }}</p>\n            {% endfor %}\n        {% endif %}\n        <p class=\"input-group required\">\n            <label for=\"email\">Email</label>\n            <input type=\"text\" name=\"email\" placeholder=\"your email address\" value=\"\" />\n            {% if form.email.errors %}\n                {% for error in form.email.errors %}\n                <p>{{ error|escape }}</p>\n                {% endfor %}\n            {% endif %}\n        </p>\n        <button type=\"button\" id=\"beeta-email-submission\" class=\"button submit\">Join the Beta</button>\n    </form>\n    <footer>\n        <p class=\"beeta-note powered-by\">\n            <em style=\"display:block; text-indent:-999em;\">Beta Powered by Beeta</em>\n            <a title=\"Visit Beeta.co\" href=\"http://www.beeta.co/\">\n            	<img alt=\"Beeta Badge\" src=\"http://samdeng.com:3000/static/images/content/beeta-badge_dark.svg\" />\n            </a>\n        </p>\n    </footer>\n{% endif %}\n";
  });
})();
