document.write('<script type="text/javascript" src="{{ SITE_URL }}/static/js/vendor/handlebars.runtime.js"></script>');
document.write('<div id="beeta-form"></div>');


var Beeta = (function() {
    var render_form_url = '{{ SITE_URL }}/signup/{{ client_id }}/render-form/';
    var process_form_url = '{{ SITE_URL }}/signup/{{ client_id }}/process-form/';

    var inject_form = function(template_url) {
        // Fetch cross-domain sign-up form
        var script = document.createElement('script');
        script.type = 'application/javascript';
        script.src = template_url;
        $('#beeta-form').append(script);

        // TODO: Use promiseJS, stealJS, or requireJS for load order enforcement
        var wait_for_handlebars_templates = setInterval(function() {
            if (typeof(Handlebars.templates) == 'undefined') { return; }
            clearInterval(wait_for_handlebars_templates);

            $('#beeta-form').html(Handlebars.templates['signup-form']);
            delete Handlebars.templates;

            $('#beeta-email-submission').click(function() {
                var data = $('#beeta-form form').serialize();
                inject_form(process_form_url + '?' + data);
            });
        }, 100);

    }

    var initialize = function() {
        var wait_for_handlebars = setInterval(function() {
            if (typeof(Handlebars) == 'undefined') { return; }
            clearInterval(wait_for_handlebars);

            inject_form(render_form_url);
        }, 100);
    }

    // Exposed interfaces
    return {
        initialize: initialize
    }
})();

Beeta.initialize();
