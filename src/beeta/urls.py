from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

from beeta.registration.views import SplashPage, RenderForm, ProcessForm, \
    TestForm, InjectForm


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', SplashPage.as_view(), name='splash'),

    # Beeta client form
    url(r'^signup/(?P<client_id>[0-9A-Za-z]+)/inject-form.js',
        InjectForm.as_view(), name='inject_form'),
    url(r'^signup/(?P<client_id>[0-9A-Za-z]+)/render-form/$',
        RenderForm.as_view(), name='render_form'),
    url(r'^signup/(?P<client_id>[0-9A-Za-z]+)/process-form/$',
        ProcessForm.as_view(), name='process_form'),
    url(r'^signup/test-form/$', TestForm.as_view(), name='test_form'),

    url(r'^admin/', include(admin.site.urls)),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,                                  
        }),
)
