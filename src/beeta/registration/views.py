from django.contrib import messages
from django.http import HttpResponseBadRequest, HttpResponseNotFound
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView

from beeta.registration.forms import AccountForm
from beeta.registration.models import Account, Client


class SplashPage(CreateView):
    form_class = AccountForm
    model = Account
    template_name = 'clients/beeta.html'
    success_url = '/'

    def form_valid(self, form):
        client_id = self.request.POST.get('client_id', None)
        if client_id is None:
            return HttpResponseBadRequest('Missing client ID.')

        try:
            form.instance.client = Client.objects.get(client_id=client_id)
        except Client.DoesNotExist:
            return HttpResponseNotFound('Invalid client ID: %s' % client_id)

        messages.add_message(self.request, messages.SUCCESS,
            'beeta-registration-complete')
        return super(SplashPage, self).form_valid(form)


# Pull in Beeta form, along with dependencies, from consumer site
class InjectForm(TemplateView):
    template_name = 'clients/inject-form.js'

    def get(self, request, client_id):
        try:
            self.client = Client.objects.get(client_id=client_id)
        except Client.DoesNotExist:
            return HttpResponseNotFound('Invalid client ID: %s' % client_id)

        return super(InjectForm, self).get(request)

    def get_context_data(self, **kwargs):
        context = super(InjectForm, self).get_context_data(**kwargs)
        context['client_id'] = self.client.client_id
        return context

    def render_to_response(self, context, **kwargs):
        kwargs['content_type'] = 'application/javascript'
        return super(InjectForm, self).render_to_response(context, **kwargs)


# Beeta form 'GET'
class RenderForm(TemplateView):
    template_name = 'clients/signup-form.js'

    def render_to_response(self, context, **kwargs):
        kwargs['content_type'] = 'application/javascript'
        return super(RenderForm, self).render_to_response(context, **kwargs)


# Beeta form 'POST'
class ProcessForm(TemplateView):
    template_name = 'clients/signup-form.js'

    def get(self, request, client_id):
        try:
            client = Client.objects.get(client_id=client_id)
        except Client.DoesNotExist:
            return HttpResponseNotFound('Invalid client ID: %s' % client_id)

        self.form = AccountForm(request.GET)
        if self.form.is_valid():
            self.form.instance.client = client
            account = self.form.save()
            return render_to_response(self.template_name, {'account': account})

        return super(ProcessForm, self).get(request)

    def render_to_response(self, context, **kwargs):
        context['form'] = self.form
        kwargs['content_type'] = 'application/javascript'
        return super(ProcessForm, self).render_to_response(context, **kwargs)


class TestForm(TemplateView):
    template_name = 'registration/test-form.html'
