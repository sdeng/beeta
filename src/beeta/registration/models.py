import uuid

from django.db import models


def generate_uuid():
    return uuid.uuid4().hex

class Client(models.Model):
    client_id = models.CharField(max_length=40, default=generate_uuid,
        unique=True)

    name = models.CharField(max_length=200, blank=True)
    email = models.EmailField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % self.name


class Account(models.Model):
    client = models.ForeignKey(Client)

    name = models.CharField(max_length=200, blank=True)
    email = models.EmailField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('client', 'email')

    def __unicode__(self):
        return u'%s (%s)' % (self.email, self.client)
