from django import forms

from beeta.registration.models import Account


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        exclude = ('client',)
