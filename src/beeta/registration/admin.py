from django.contrib import admin

from beeta.registration.models import Account, Client


class AccountAdmin(admin.ModelAdmin):
    model = Account
    list_filter = ('client',)

class ClientAdmin(admin.ModelAdmin):
    model = Client


admin.site.register(Account, AccountAdmin)
admin.site.register(Client, ClientAdmin)
